import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { AboutPage } from '../about/about';

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {

  rootPage = ProfilePage

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openProfile() {
    this.navCtrl.push(ProfilePage)
  }

  openAbout() {
    this.navCtrl.push(AboutPage)
  }
}
