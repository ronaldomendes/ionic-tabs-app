import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';

@IonicPage()
@Component({
  selector: 'page-movie-details',
  templateUrl: 'movie-details.html',
  providers: [MovieProvider]
})
export class MovieDetailsPage {

  public movie
  public movieId

  constructor(public navCtrl: NavController, public navParams: NavParams, private provider: MovieProvider) {
  }

  ionViewDidEnter() {
    this.movieId = this.navParams.get('id')
    this.provider.getMovieDetails(this.movieId).subscribe(data => this.movie = data, err => console.log(err))
  }

}
