import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';
import { MovieDetailsPage } from '../movie-details/movie-details';

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
  providers: [MovieProvider]
})

export class FeedPage {

  public movieList = new Array<any>()
  public loader
  public refresher
  public isRefreshing: boolean = false
  public page: number = 1
  public infiniteScroll

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private movie: MovieProvider, private loadCtrl: LoadingController) {
  }

  doRefresh(event) {
    this.page = 1
    this.refresher = event
    this.isRefreshing = true
    this.loadMovies()
  }

  presentLoading() {
    this.loader = this.loadCtrl.create({
      content: 'Loading...'
    })
    this.loader.present()
  }

  closeLoading() {
    this.loader.dismiss()
  }

  ionViewDidEnter() {
    this.page = 1
    this.loadMovies()
  }

  loadMovies(newPage: boolean = false) {
    this.presentLoading()
    this.movie.getLatestMovies(this.page).subscribe(data => {

      if (newPage) {
        this.movieList = this.movieList.concat(data['results'] as any)
        this.infiniteScroll.complete()
      } else {
        this.movieList = data['results'] as any
      }

      this.closeLoading()

      if (this.isRefreshing) {
        this.refresher.complete()
        this.isRefreshing = false
      }
    }, err => {
      console.log(err)
      this.closeLoading()

      if (this.isRefreshing) {
        this.refresher.complete()
        this.isRefreshing = false
      }
    })
  }

  openDetails(movie) {
    this.navCtrl.push(MovieDetailsPage, { id: movie.id })
  }

  doInfinite(event) {
    this.page++
    this.infiniteScroll = event
    this.loadMovies(true)
  }
}
