import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MovieProvider {

  private baseUrl: string = 'https://api.themoviedb.org/3/movie'
  private apiKeyV3: string = ''

  constructor(public http: HttpClient) { }

  getLatestMovies(page = 1) {
    return this.http.get(`${this.baseUrl}/popular?page=${page}&api_key=${this.apiKeyV3}`)
  }

  getMovieDetails(movie_id: number) {
    return this.http.get(`${this.baseUrl}/${movie_id}?api_key=${this.apiKeyV3}`)
  }

}
