import { Injectable } from '@angular/core';

@Injectable()
export class ConfigProvider {

  private config = {
    showSlide: false,
    name: '',
    username: ''
  }

  constructor() { }

  getConfigData() {
    return localStorage.getItem("config")
  }

  setConfigData(showSlide?: boolean, name?: string, username?: string) {

    if (showSlide) {
      this.config.showSlide = showSlide
    }
    
    if (name) {
      this.config.name = name
    }
    
    if (username) {
      this.config.username = username
    }

    localStorage.setItem("config", JSON.stringify(this.config))
  }

}
